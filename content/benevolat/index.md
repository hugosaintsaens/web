+++
title = 'Bénévolat'
date = 2024-03-30T15:50:23+01:00
draft = false
layout = 'single'
+++

## Performance Builders

{{< timeline >}}

{{< timelineVolunteerismItem icon="briefcase-solid" imageSrc="performance-builders.png" imageAlt="Identité visuelle de Performance Builders" imageTitle="Identité visuelle de Performance Builders" badge="02/2024 - 03/2024" job="Développeur web" location="Toulouse, Occitanie, France" website="https://performance-builders.fr" >}}
<h4>Site web statique de préparation physique</h4>
<ul>
  <li>Première utilisation d’un framework Front-End React : Next.js</li>
  <li>Utilisation du framework Tailwind CSS</li>
  <li>Test unitaire avec Vitest</li>
  <li>Test d'intégration avec Playwright</li>
  <li>Garantir la satisfaction client</li>
</ul>
{{< /timelineVolunteerismItem >}}

{{< /timeline >}}

## Fernand MICO

{{< timeline >}}

{{< timelineVolunteerismItem icon="briefcase-solid" imageSrc="fernand-mico.png" imageAlt="Identité visuelle de Fernand MICO" imageTitle="Identité visuelle de Fernand MICO" badge="10/2023 - 01/2024" job="Développeur web" location="Toulouse, Occitanie, France" website="https://fernand-mico.fr" >}}
<h4>Seconde refonte du site web biographique</h4>
<ul>
  <li>Mise en place d'une architecture micro-services</li>
  <li>Première utilisation d’un framework Front-End : Svelte</li>
  <li>Première utilisation du framework Tailwind CSS</li>
  <li>Test d'intégration avec Playwright</li>
  <li>Utilisation de Laravel pour l'API</li>
  <li>Amélioration de l'interface graphique</li>
  <li>Garantir la satisfaction client</li>
</ul>
{{< /timelineVolunteerismItem >}}

{{< timelineVolunteerismItem icon="briefcase-solid" imageSrc="fernand-mico.png" imageAlt="Identité visuelle de Fernand MICO" imageTitle="Identité visuelle de Fernand MICO" badge="04/2020 - 07/2020" job="Développeur web" location="Toulouse, Occitanie, France" website="https://fernand-mico.fr" >}}
<h4>Première refonte du site web biographique</h4>
<ul>
  <li>Création d’une identité visuelle</li>
  <li>Amélioration de l’expérience utilisateur</li>
  <li>Première utilisation d’un framework CSS : Bootstrap</li>
  <li>Garantir la satisfaction client</li>
</ul>
{{< /timelineVolunteerismItem >}}

{{< /timeline >}}
