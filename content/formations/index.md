+++
title = 'Formations'
date = 2024-03-30T14:50:12+01:00
draft = false
layout = 'single'
+++

## 3iL Ingénieurs

{{< timeline >}}

{{< timelineEducationItem icon="graduation-cap" imageSrc="3il-ingenieurs.png" imageAlt="Identité visuelle de 3iL Ingénieurs" imageTitle="Identité visuelle de 3iL Ingénieurs" badge="09/2021 - 09/2024" degree="Diplôme d'Ingénieur" title="Informatique" location="Rodez, Occitanie, France" website="https://www.3il-ingenieurs.fr/ingenieur-3il-en-alternance/" >}}
<p>Test of English for International Communication (TOEIC) : Niveau <span class="font-bold">B2</span> (score 805/990)</p>
{{< /timelineEducationItem >}}

{{< /timeline >}}


## Haute École en Hainaut

{{< timeline >}}

{{< timelineEducationItem icon="graduation-cap" imageSrc="haute-ecole-en-hainaut.png" imageAlt="Identité visuelle de Haute École en Hainaut" imageTitle="Identité visuelle de Haute École en Hainaut" badge="09/2023 - 01/2024" degree="Master" title="Sciences de l'Ingénieur Industriel en Informatique" location="Mons, Wallonie, Belgique" website="https://www.heh.be/master-ingenieur-en-informatique" >}}
Orientation Réseaux et Sécurité 
{{< /timelineEducationItem >}}

{{< /timeline >}}

## Université Paul Sabatier

{{< timeline >}}

{{< timelineEducationItem icon="graduation-cap" imageSrc="universite-paul-sabatier.png" imageAlt="Identité visuelle de Université Paul Sabatier" imageTitle="Identité visuelle de Université Paul Sabatier" badge="09/2020 - 08/2021" degree="Licence Professionnelle" title="Gestion et Traitement Informatique de Données Massives" location="Toulouse, Occitanie, France" website="https://www.univ-tlse3.fr/medias/fichier/lp-info-gtid_1626356422535-pdf" >}}
{{< /timelineEducationItem >}}

{{< timelineEducationItem icon="graduation-cap" imageSrc="universite-paul-sabatier.png" imageAlt="Identité visuelle de Université Paul Sabatier" imageTitle="Identité visuelle de Université Paul Sabatier" badge="09/2017 - 06/2020" degree="Diplôme Universitaire de Technologie" title="Informatique" location="Toulouse, Occitanie, France" website="https://iut.univ-tlse3.fr/bachelor-specialite-informatique" >}}
Favorable pour une poursuite d'études longues
{{< /timelineEducationItem >}}

{{< /timeline >}}

## Lycée Saint-Exupéry

{{< timeline >}}

{{< timelineEducationItem icon="graduation-cap" imageSrc="lycee-saint-exupery.png" imageAlt="Identité visuelle de Lycée Saint-Exupéry" imageTitle="Identité visuelle de Lycée Saint-Exupéry" badge="09/2015 - 06/2017" degree="Baccalauréat" title="Sciences et technologies de l'industrie et du développement durable" location="Blagnac, Occitanie, France" >}}
Mention Bien
{{< /timelineEducationItem >}}

{{< /timeline >}}
