+++
title = 'Mentions légales'
date = 2024-03-31T21:18:14+02:00
draft = false
layout = 'single'
+++

## Responsable

Le site web « [www.hugo-saintsaens.fr](https://www.hugo-saintsaens.fr) » est détenu et administré par Hugo SAINT-SAËNS.

## Hébergeur

[Infomaniak Network SA](https://www.infomaniak.com/fr) - 25 Rue Eugène Marziano, 1227 Les Acacias, Suisse.