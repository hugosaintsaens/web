+++
title = 'Compétences'
date = 2024-03-24T11:14:55+01:00
draft = false
layout = 'single'
+++

## Logiciel

- **C**
- **Python**
	- pandas
	- NumPy
	- Matplotlib
	- seaborn
	- BeautifulSoup
	- scikit-learn
- **Java**
	- Swing
- **Go**
- **Rust**
- **C#**
- **Ada**
- **Assembleur**

## Web

### Front-End

- **HTML**
- **CSS**
	- Tailwind CSS
	- Bootstrap
- **JavaScript**
- **TypeScript**
	- Svelte (SvelteKit)
	- React (Next.js)
- **Hugo Template**

### Back-End

- **Node.js**
	- AdonisJS
	- Express.js
- **PHP**
	- Symfony
	- Laravel

### Test

- **Vitest**
- **Playwright**
- **PHPUnit**
- **Japa**

### Service

- **REST**
	- Insomnia
- **SOAP**

## Conteneurisation

- **Docker**

## DevSecOps

- **Jenkins**
- **Jira**
- **SonarQube**
- **HashiCorp**

## Base de données relationnelle

- **Oracle**
- **MySQL**
- **MariaDB**
- **PostgreSQL**
- **SQLite**
- **MS Access**

## Base de données non relationnelle

- **MongoDB**
- **Neo4J**

## Big Data

- **Hadoop**
- **Talend**

## Réseau

### Architecture

- **Cisco Packet Tracer**

### SSH & Telnet

- **PuTTy**
- **MobaXterm**

### FTP

- **FileZilla**

## Virtualisation

- **VMWare**
- **VirtualBox**

## Script

- **Bash**
- **PowerShell**
- **Google Apps Script**

## Balisage

- **XML**

## Modélisation 

- **UML**
- **MERISE**

## Gestion de version

- **Git**
	- GitHub
	- GitLab
	- Gitea
- **SVN**

## Gestion de projet

- **En cascade**
- **Scrum**
- **Kanban**