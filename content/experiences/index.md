+++
title = 'Expériences'
date = 2024-03-24T17:18:32+01:00
draft = false
layout = 'single'
+++

## Domaine d'expertise

### Airbus

{{< timeline >}}

{{< timelineExperienceItem icon="briefcase-solid" imageSrc="airbus.png" imageAlt="Identité visuelle de Airbus" imageTitle="Identité visuelle de Airbus" badge="09/2021 - 09/2024" subheader="Apprentissage ⋅ Développeur web" location="Toulouse, Occitanie, France" >}}
<h4>Tableau de bord pour gérer le planning avions du pôle peinture de Toulouse</h4>
<ul>
  <li>Migration d'une base de données sans clés et relations vers une relationnelle</li>
  <li>Importation de données prévisionnelles via des classeurs Excel</li>
  <li>Analyse et conception en se basant sur l'existant</li>
  <li>Possibilité d’éditer des créneaux et décorations avions</li>
  <li>Gestion des utilisateurs et de leurs rôles associés</li>
  <li>Administration complète des options de l’application</li>
  <li>Visualisation d'Indicateurs Clés de Performance</li>
  <li>Architecture Modèle-Vue-Contrôleur en PHP natif</li>
</ul>
<h4 class="mt-8">Tableau de bord pour suivre les demandes outillages, utilisés lors des cycles peintures</h4>
<ul>
  <li>Création d’une base de données relationnelle en se basant sur les besoins clients</li>
  <li>Importation de données issues de SAP via des fichiers textes CSV</li>
  <li>Affichage des états des demandes et si des actions sont nécessaires</li>
  <li>Allocation des outillages à des budgets et catégories spécifiques</li>
  <li>Visualisation d'Indicateurs Clés de Performance</li>
  <li>Architecture monolithique en PHP natif</li>
</ul>
{{< /timelineExperienceItem >}}

{{< /timeline >}}

### Groupe GAMBA

{{< timeline >}}

{{< timelineExperienceItem icon="briefcase-solid" imageSrc="groupe-gamba.png" imageAlt="Identité visuelle de Groupe GAMBA" imageTitle="Identité visuelle de Groupe GAMBA" badge="03/2021 - 08/2021" subheader="Stage · Développeur logiciel" location="Labège, Occitanie, France" >}}
<h4>Recherche d'un nouveau système de base de données conforme aux besoins</h4>
<ul>
  <li>État des lieux et comparaisons de plusieurs bases de données relationnelles</li>
  <li>Migration de Access 95 au profit de SQLite</li>
  <li>Analyse d'une application .NET préexistante</li>
  <li>Adapté l'utilisation de SQLite à l'application</li>
</ul>
{{< /timelineExperienceItem >}}

{{< /timeline >}}

## Saisonniers

### Basic-Fit

{{< timeline >}}

{{< timelineExperienceItem icon="briefcase-solid" imageSrc="basic-fit.png" imageAlt="Identité visuelle de Basic-Fit" imageTitle="Identité visuelle de Basic-Fit" badge="07/2020 - 08/2020" subheader="CDD · Agent d'accueil" location="Ramonville-Saint-Agne, Occitanie, France" >}}
<h4>Responsable du club de sports</h4>
<ul>
  <li>Accueil et accompagnement des adhérents</li>
  <li>Vente d'abonnements et de produits nutrition</li>
  <li>Sauvegarde d'un environnement propre</li>
  <li>Exécuter un processus de sécurité lors de l'ouverture et la fermeture du club</li>
</ul>
{{< /timelineExperienceItem >}}

{{< /timeline >}}

### AB7 Industries

{{< timeline >}}

{{< timelineExperienceItem icon="briefcase-solid" imageSrc="ab7-industries.png" imageAlt="Identité visuelle de AB7 Industries" imageTitle="Identité visuelle de AB7 Industries" badge="07/2019 - 08/2019" subheader="CDD · Agent de production" location="Deyme, Occitanie, France" >}}
<h4>Participation à la fabrication de produits vétérinaires</h4>
<ul>
  <li>Conditionnement de produits et vérification de leurs qualités</li>
  <li>Utilisation et maintenance de machines industrielles</li>
  <li>Travail sur différents postes de production</li>
</ul>
{{< /timelineExperienceItem >}}

{{< /timeline >}}

### Intermarché

{{< timeline >}}

{{< timelineExperienceItem icon="briefcase-solid" imageSrc="intermarche.png" imageAlt="Identité visuelle de Intermarché" imageTitle="Identité visuelle de Intermarché" badge="07/2018" subheader="CDD · Employé commercial" location="Saint-Jory, Occitanie, France" >}}
<h4>Responsable de plusieurs rayons</h4>
<ul>
  <li>Gestion de rayons secs et de leurs stocks associés</li>
  <li>Accompagnement auprès des clients</li>
  <li>Assistance à mes collaborateurs</li>
</ul>
{{< /timelineExperienceItem >}}

{{< timelineExperienceItem icon="briefcase-solid" imageSrc="intermarche.png" imageAlt="Identité visuelle de Intermarché" imageTitle="Identité visuelle de Intermarché" badge="07/2017" subheader="CDD · Employé commercial" location="Saint-Jory, Occitanie, France" >}}
<h4>Responsable de plusieurs rayons</h4>
<ul>
  <li>Activité pendant l'agrandissement et la restructuration du magasin</li>
  <li>Gestion de rayons surgelés et frais ainsi que leurs stocks associés</li>
  <li>Accompagnement auprès des clients</li>
  <li>Assistance à mes collaborateurs</li>
</ul>
{{< /timelineExperienceItem >}}

{{< /timeline >}}

### Marc TERRISSE

{{< timeline >}}

{{< timelineExperienceItem icon="briefcase-solid" imageSrc="empty.png" imageAlt="Aucune identité visuelle disponible" imageTitle="Aucune identité visuelle disponible" badge="07/2016 - 08/2016" subheader="CDD · Ouvrier agricole" location="Villematier, Occitanie, France" >}}
<h4>Écimage du maïs avec une équipe de saisonniers</h4>
<ul>
  <li>Responsable du contrôle des champs</li>
</ul>
{{< /timelineExperienceItem >}}

{{< /timeline >}}