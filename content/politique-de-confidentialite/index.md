+++
title = 'Politique de confidentialité'
date = 2024-03-31T21:18:54+02:00
draft = false
layout = 'single'
+++

Le site web « [www.hugo-saintsaens.fr](https://www.hugo-saintsaens.fr) » ne collecte aucune donnée à caractère personnel ou non.