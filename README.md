# Hugo SAINT-SAËNS Web
## Description
Welcome to the Hugo SAINT-SAËNS Web.

## Table of Contents
- [Description](#description)
- [Table of Contents](#table-of-contents)
- [Technical Stack](#technical-stack)
- [Tools](#tools)
- [VSCode Extensions](#vscode-extensions)
- [License](#license)

## Technical Stack
- **Static Site Generator** : [HUGO](https://gohugo.io/)
- **Theme** : [Blowfish](https://blowfish.page/)

## Tools
- **Code Editor** : [Visual Studio Code](https://code.visualstudio.com/)

## VSCode Extensions
- **Icons** : [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)

## License
No License. All Rights Reserved.
